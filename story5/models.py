from django.db import models

# Create your models here.
class Irs(models.Model):
    Matkul = models.CharField(max_length=50)
    Dosen = models.TextField(max_length=50)
    Sks = models.IntegerField()
    Deskripsi = models.TextField(max_length=50)
    TahunAjaran = models.TextField(max_length=50)
    Ruangan = models.TextField(max_length=50)