from django.shortcuts import render,redirect
from .models import Irs as pelajaran
from .forms import IrsForm
# Create your views here.

template='form.html'

def Join(request):
    if request.method == "POST":
        form = IrsForm(request.POST)
        if form.is_valid():
            kuliah = pelajaran()
            kuliah.Matkul = form.cleaned_data['Matkul']
            kuliah.Dosen = form.cleaned_data['Dosen']
            kuliah.Sks = form.cleaned_data['Sks']
            kuliah.Deskripsi = form.cleaned_data['Deskripsi']
            kuliah.TahunAjaran = form.cleaned_data['TahunAjaran']
            kuliah.Ruangan = form.cleaned_data['Ruangan']
            kuliah.save()
        return redirect('/matkul/')
    else:
        kuliah = pelajaran.objects.all()
        form = IrsForm()
        response = {"kuliah":kuliah, 'form' : form}
        return render(request,template,response)

def dynamic_lookup_view(request, my_id):
    obj=pelajaran.objects.get(id=my_id)
    kuliah=pelajaran.objects.filter(id=my_id)
    context = {"object":obj, "kuliah": kuliah}
    return render(request, 'profile.html', context)

def jadwal_delete(request, pk):
    if request.method == "POST":
        form = IrsForm(request.POST)
        if form.is_valid():
            kuliah = pelajaran()
            kuliah.Matkul = form.cleaned_data['Matkul']
            kuliah.Dosen = form.cleaned_data['Dosen']
            kuliah.Sks = form.cleaned_data['Sks']
            kuliah.Deskripsi = form.cleaned_data['Deskripsi']
            kuliah.TahunAjaran = form.cleaned_data['TahunAjaran']
            kuliah.Ruangan = form.cleaned_data['Ruangan']
            kuliah.save()
        return redirect('/matkul/')
    else:
        pelajaran.objects.filter(pk=pk).delete()
        data = pelajaran.objects.all()
        form = IrsForm()
        response = {"kuliah":data, 'form' : form}
        return render(request, 'form.html', response)