from django import forms

class IrsForm(forms.Form):

    Matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Matkul',
        'type' : 'text',
        'required': True,
    }))

    Dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required': True,
    }))

    Sks = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'number',
        'required': True,
    }))

    Deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Deskripsi Matkul',
        'required': True,
    }))

    TahunAjaran = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Tahun Ajaran',
        'required': True,
    }))

    Ruangan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nomor Ruangan',
        'required': True,
    }))