from django.urls import path
from .views import Join, jadwal_delete, dynamic_lookup_view 
from . import views

appname = 'story5'

urlpatterns = [
   path('', Join, name = 'matkul'),
   path('<int:pk>/delete',jadwal_delete, name = 'Delete'),
   path('<int:my_id>/', dynamic_lookup_view, name='profile'),
]